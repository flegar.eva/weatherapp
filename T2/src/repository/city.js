export const cities = [
    { 
        city: 'MURSKA SOBOTA',
        lat: 46.658138,
        lon: 16.161029
    },
    {
        city: 'MARIBOR',
        lat: 46.554650,
        lon: 15.645881
    },
    {
        city: 'LJUBLJANA', 
        lat: 46.056946,
        lon: 14.505751
    },
    {
        city: 'CELJE',
        lat: 46.239749,
        lon: 15.267706
    },
    {
        city: 'PORTOROŽ',
        lat: 45.514290,
        lon: 13.590846
    },
]