export default function transformWeahterData5Days(weatherCity5Days) {
    const weatherDataTransformed = [];
    for(const wc5d of weatherCity5Days) {
        let wdt = {
            temperatura : Math.floor(wc5d.main.temp),
            vlaznost: wc5d.main.humidity,
            pritisk:  wc5d.main.pressure,
            opis: wc5d.weather[0].description,
            datum: wc5d.dt_txt,
            ikona: "https://openweathermap.org/img/w/" + wc5d.weather[0].icon + ".png"
        }
        console.log('WDT', wdt);
        weatherDataTransformed.push(wdt);
    }
    return weatherDataTransformed;
}

// var myDate = new Date('myDateString'); //you can also do milliseconds instead of the date string
// var myEuroDate = myDate.getDate() + '/' + myDate.getMonth + '/' + myDate.getFullYear();