import { cities } from '../repository/city'
import axios from 'axios';
import transformWeahterData5Days from '../helper/weather'

export default async function getWeather5Days(city) {
    let searchedCity;
    try{
        searchedCity = cities.filter((value) => value.city === city.toUpperCase());
    }
    catch (error) {
        console.error('[NAPAKA]', error);
    }
    console.log('searchedCityyyyy:', searchedCity);

    if (searchedCity === []) {
        console.log('searchedCity is empty');
        throw new Error('Mesto ne obstaja!');
    }
    searchedCity = searchedCity[0]

    console.log('searchedCity:', searchedCity, 'searchedCity.lat:', searchedCity.lat, 'searchedCity.lon:', searchedCity.lon)

    const response = await axios.get(`http://api.openweathermap.org/data/2.5/forecast?lat=${searchedCity.lat}&lon=${searchedCity.lon}&appid=7afaf381bd3034ee03becfbd630d0363`)
    console.log('RESPONSE:', response)

    const vreme = response.data.list.slice(0, 5);

    console.log('VREME:', vreme)

    return transformWeahterData5Days(vreme);
}

